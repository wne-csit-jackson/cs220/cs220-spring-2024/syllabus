### Extenuating Circumstances

If a circumstance beyond your control causes you to violate the above policies,
you may request special considerations for your circumstance. Make a request as
follows:

1. Complete the Extenuating Circumstance "quiz" on Kodiak.
2. Contact the instructor by Discord or email.

Why 2 methods of contact? It helps ensure that your request is received.
Also, the Kodiak "quiz" provides a formal way for me to track requests.
However, it does not provide a mechanism for 2-way communication.
So it's still important to communicate with me over Discord or email.

Requests must be submitted as soon as you are aware of the extenuating
circumstances. Your instructor will determine the acceptability of
a request, along with the specific accommodations that will be made.

_If you have any problems with the above instructions, please discuss your
situation with your instructor._
